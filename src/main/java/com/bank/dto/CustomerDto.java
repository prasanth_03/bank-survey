package com.bank.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
	@NotBlank(message = "First Name is required field")
	private String firstName;
	
	@NotBlank(message = "Last Name is required field")
	private String lastName;
	
	@NotBlank(message = "Address is required field")
	private String address;
	
	@NotBlank(message = "Gender is required field")
	private String gender;
}
