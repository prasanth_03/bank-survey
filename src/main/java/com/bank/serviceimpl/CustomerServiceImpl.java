package com.bank.serviceimpl;
import java.util.Collections;
import java.util.Optional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.bank.dto.CustomerDto;
import com.bank.dto.ResponseDTO;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.UserAlreadyExists;
import com.bank.repository.CustomerRepository;
 
 
@Service
public class CustomerServiceImpl {
 
	@Autowired
	private CustomerRepository registrationRepository;
	public CustomerServiceImpl(CustomerRepository registrationRepository) {
		super();
		this.registrationRepository = registrationRepository;
	}
	public ResponseDTO registration(CustomerDto registrationdto) {
		Optional<CustomerRegistration> userRegistration = registrationRepository.findByFirstName(registrationdto.getFirstName());
		if (userRegistration.isPresent()) {
			throw new UserAlreadyExists("Customer already Registered");
		}
		CustomerRegistration registration = new CustomerRegistration();
		registration.setFirstName(registrationdto.getFirstName());
		registration.setLastName(registrationdto.getLastName());
		registration.setGender(registrationdto.getGender());
		registration.setAddress(registrationdto.getAddress());
		registrationRepository.save(registration);
		return new ResponseDTO(Collections.singletonList("Customer Registration successfully..."), 201);
	}
}