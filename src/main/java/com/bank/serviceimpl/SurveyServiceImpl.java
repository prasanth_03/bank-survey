package com.bank.serviceimpl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bank.entity.Servey;
import com.bank.repository.ServeyRepository;
import com.bank.service.ServeyService;


import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SurveyServiceImpl implements ServeyService {

	private final ServeyRepository repository;
	
	@Override
	public List<Servey> getAll() {
		
		return repository.findAll();
	}

}

