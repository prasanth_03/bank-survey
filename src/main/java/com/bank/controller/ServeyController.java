package com.bank.controller;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.entity.Servey;
import com.bank.service.ServeyService;
import com.opencsv.ICSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class ServeyController {

	private final ServeyService surveyService;
	
	@GetMapping("/export")
	public void getData(HttpServletResponse response) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		
		String fileName = "Survey-export.csv";
		
		response.setContentType("text/csv");
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+fileName + "\"");
		
		
		StatefulBeanToCsv<Servey> writer = 
	            new StatefulBeanToCsvBuilder<Servey>
	                 (response.getWriter())
	                 .withQuotechar(ICSVWriter.NO_QUOTE_CHARACTER)
	                   .withSeparator(ICSVWriter.DEFAULT_SEPARATOR)
	            .withOrderedResults(false).build();
		
		writer.write(surveyService.getAll());

		 
		
	}
}
