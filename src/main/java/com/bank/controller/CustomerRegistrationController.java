package com.bank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bank.dto.CustomerDto;
import com.bank.dto.ResponseDTO;
import com.bank.serviceimpl.CustomerServiceImpl;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
 
@AllArgsConstructor
@RestController
@RequestMapping()
public class CustomerRegistrationController {
 
	CustomerServiceImpl registrationServiceImpl;
	@PostMapping("/customer")
	public ResponseEntity<ResponseDTO> register(@Valid @RequestBody CustomerDto resgistrationDto) {
		return new ResponseEntity<ResponseDTO>(registrationServiceImpl.registration(resgistrationDto),
				HttpStatus.CREATED);
	}
}