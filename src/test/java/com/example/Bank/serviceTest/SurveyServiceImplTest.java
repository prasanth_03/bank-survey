package com.example.Bank.serviceTest;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bank.entity.Servey;
import com.bank.repository.ServeyRepository;
import com.bank.serviceimpl.SurveyServiceImpl;

@ExtendWith(MockitoExtension.class)
class SurveyServiceImplTest {

	@Mock
	private ServeyRepository repository;

	@InjectMocks
	private SurveyServiceImpl service;

	@Test
	void testGetAll() {

		Servey survey1 = new Servey(1L, "customerId1", "branchId1", "distance1");
		Servey survey2 = new Servey(2L, "customerId2", "branchId2", "distance2");

		List<Servey> surveys = Arrays.asList(survey1, survey2);

		when(repository.findAll()).thenReturn(surveys);
		List<Servey> result = service.getAll();

		verify(repository).findAll();
		assertEquals(surveys.size(), result.size());
		assertTrue(result.contains(survey1));
		assertTrue(result.contains(survey2));
	}

	@Test
	void testGetAllEmptyList() {

		List<Servey> surveys = Arrays.asList();

		when(repository.findAll()).thenReturn(surveys);
		List<Servey> result = service.getAll();

		verify(repository).findAll();
		assertEquals(0, result.size());
	}

	@Test
	void testGetAllSurveys() {
		List<Servey> surveyList = new ArrayList<>();
		surveyList.add(new Servey(1L, "customerId1", "branchId1", "10 km"));
		surveyList.add(new Servey(2L, "customerId2", "branchId2", "15 km"));

		when(repository.findAll()).thenReturn(surveyList);

		List<Servey> result = service.getAll();

		Assertions.assertEquals(2, result.size());

	}

}