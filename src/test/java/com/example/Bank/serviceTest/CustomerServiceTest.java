package com.example.Bank.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bank.dto.CustomerDto;
import com.bank.dto.ResponseDTO;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.UserAlreadyExists;
import com.bank.repository.CustomerRepository;
import com.bank.serviceimpl.CustomerServiceImpl;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

	@Mock
	private CustomerRepository customerRepository;

	@InjectMocks
	private CustomerServiceImpl customerService;

	@Test
	void testCustomerRegistration_Success() {

		CustomerDto customerDto = new CustomerDto("Thamarai", "selvi", "123 Main St", "FeMale");
		when(customerRepository.findByFirstName(customerDto.getFirstName())).thenReturn(Optional.empty());

		ResponseDTO responseDTO = customerService.registration(customerDto);

		assertNotNull(responseDTO);
		assertEquals(201, responseDTO.getCode());
		assertEquals("Customer Registration successfully...", responseDTO.getMessage().get(0));

		verify(customerRepository, times(1)).findByFirstName(customerDto.getFirstName());
		verify(customerRepository, times(1)).save(ArgumentMatchers.any(CustomerRegistration.class));
	}

	@Test
	void testCustomerRegistration_UserAlreadyExists() {

		CustomerDto customerDto = new CustomerDto("Thamarai", "selvi", "123 Main St", "FeMale");
		when(customerRepository.findByFirstName(customerDto.getFirstName()))
				.thenReturn(Optional.of(new CustomerRegistration()));

		assertThrows(UserAlreadyExists.class, () -> customerService.registration(customerDto));

		verify(customerRepository, times(1)).findByFirstName(customerDto.getFirstName());
		verify(customerRepository, times(0)).save(ArgumentMatchers.any(CustomerRegistration.class));
	}
}
