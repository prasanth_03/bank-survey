package com.example.Bank.controllerTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bank.controller.CustomerRegistrationController;
import com.bank.dto.CustomerDto;
import com.bank.dto.ResponseDTO;
import com.bank.serviceimpl.CustomerServiceImpl;

@ExtendWith(MockitoExtension.class)
class CustomerRegistrationControllerTest {
	@Mock
	private CustomerServiceImpl registrationServiceImpl;
	@InjectMocks
	private CustomerRegistrationController registrationController;

	@Test
	void register_Successful() {
		CustomerDto registrationDto = new CustomerDto("John", "Doe", "Address", "Male");
		when(registrationServiceImpl.registration(registrationDto)).thenReturn(new ResponseDTO());
		ResponseEntity<ResponseDTO> responseEntity = registrationController.register(registrationDto);

		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());

		verify(registrationServiceImpl, times(1)).registration(registrationDto);
	}

	@Test
	void register_NullInput() {
		ResponseEntity<ResponseDTO> responseEntity = registrationController.register(null);

		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertNull(responseEntity.getBody());
	}

	@Test
	void register_CustomAssertion() {
		CustomerDto registrationDto = new CustomerDto("John", "Doe", "Address", "Male");
		ResponseDTO mockResponse = new ResponseDTO();
		mockResponse.setCode(200);
		mockResponse.setMessage(List.of("Mock Message"));
		when(registrationServiceImpl.registration(registrationDto)).thenReturn(mockResponse);
		ResponseEntity<ResponseDTO> responseEntity = registrationController.register(registrationDto);
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals(200, responseEntity.getBody().getCode());
		assertEquals("Mock Message", responseEntity.getBody().getMessage().get(0));
		verify(registrationServiceImpl, times(1)).registration(registrationDto);
	}
}