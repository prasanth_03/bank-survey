package com.example.Bank.controllerTest;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bank.controller.ServeyController;
import com.bank.entity.Servey;
import com.bank.service.ServeyService;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import jakarta.servlet.http.HttpServletResponse;

@ExtendWith(MockitoExtension.class)
class ServeyControllerTest {

	@Mock
	private ServeyService service;

	@InjectMocks
	private ServeyController controller;

	@Mock
	private HttpServletResponse response;

	private MockMvc mockMvc;

	@Test
	void testGetData() throws Exception {

		Servey survey1 = new Servey(1L, "customerId1", "branchId1", "distance1");
		Servey survey2 = new Servey(2L, "customerId2", "branchId2", "distance2");

		List<Servey> surveys = Arrays.asList(survey1, survey2);

		when(service.getAll()).thenReturn(surveys);

		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

		mockMvc.perform(get("/export").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

		verify(service).getAll();
	}

	@Test
	void testGetDataPositiveScenario()
			throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

		doNothing().when(response).setContentType(anyString());
		doNothing().when(response).setHeader(anyString(), anyString());
		when(service.getAll()).thenReturn(Collections.emptyList());
		controller.getData(response);
	}

}